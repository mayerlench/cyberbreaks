﻿new Vue({
    el: "#index",
    data: {
        addNewLogin: false,
        logins:[],
        login: {
            Website: "",
            Username: "",
            Password: "",
            StartDate: "",
            EndDate: ""
        }
    },
    ready: function () {
        $('.datepicker-input').datepicker();

    },
    methods: {
        addNewLoginBtn: function () {
            this.addNewLogin = true;
            $(".modal").modal();
        },

        submitNewLogin: function () {
            var self = this;
            if (!this.login.Website || !this.login.Username || !this.login.Password || !this.login.StartDate || !this.login.EndDate) {
                return;
            }
            $.post("/home/addNewLogin", { l: this.login }, function (result) {
                if (result != null) {
                    self.logins.push(result);
                    $(".modal").modal("hide");
                    self.login = {};
                }
            });
        },

    }
});