﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cyberbreaks.Data;
using CyberbreaksLogins.Models;

namespace CyberbreaksLogins.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var loginsRepo = new LoginsRepo(Properties.Settings.Default.Constr);
            var userRepo = new UserLoginsRepo(Properties.Settings.Default.Constr);
            var vm = new IndexViewModel { Logins = loginsRepo.GetLogins(), User = userRepo.GetUser(User.Identity.Name) };
            return View(vm);
        }

        [HttpPost]
        public ActionResult AddNewLogin(Login l)
        {
            var loginsRepo = new LoginsRepo(Properties.Settings.Default.Constr);
            var userRepo = new UserLoginsRepo(Properties.Settings.Default.Constr);
            var user = userRepo.GetUser(User.Identity.Name);
            if (user.IsAdmin)
            {
                l.UserId = user.Id;
                loginsRepo.AddLogin(l);
                return Json(new LoginJson
                {
                    Id = l.Id,
                    Website = l.Website,
                    Username = l.Username,
                    Password = l.Password,
                   DaysRemaining = l.EndDate > l.StartDate ? (int)l.EndDate.Subtract(DateTime.Now).TotalDays : 0
                });
            }
            return Json(false);
        }
    }
}