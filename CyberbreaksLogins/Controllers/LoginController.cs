﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Cyberbreaks.Data;

namespace CyberbreaksLogins.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string username, string password)
        {
            var userRepo = new UserLoginsRepo(Properties.Settings.Default.Constr);
            if (userRepo.Login(username, password) != null)
            {
                FormsAuthentication.SetAuthCookie(username, false);
                return Redirect("/home/index");
            }
            return View((object) "Incorrect Username Or Password");
        }
    }
}