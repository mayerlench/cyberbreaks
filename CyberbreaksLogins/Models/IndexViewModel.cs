﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cyberbreaks.Data;

namespace CyberbreaksLogins.Models
{
    public class IndexViewModel
    {
        public IEnumerable<Login> Logins  { get; set; }
        public UserLogin User  { get; set; }
    }
}