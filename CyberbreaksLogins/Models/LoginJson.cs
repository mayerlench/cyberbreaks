﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CyberbreaksLogins.Models
{
    public class LoginJson
    {
        public int Id { get; set; }
        public string Website { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int DaysRemaining { get; set; }
        public int UserId { get; set; }
    }
}