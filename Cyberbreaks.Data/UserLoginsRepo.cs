﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyberbreaks.Data
{
   public class UserLoginsRepo
    {
        private string _connectionString;

        public UserLoginsRepo(string connectionString)
        {
            _connectionString = connectionString;
        }

        public UserLogin CheckIfAvailable(string username)
        {
            using (var context = new CyberbreaksDBDataContext(_connectionString))
            {
                return context.UserLogins.FirstOrDefault(p => p.Username == username);
            }
        }
        public void AddUser(string username, string password,bool isAdmin)
        {
            string salt = PasswordHelper.GenerateRandomSalt();
            string hash = PasswordHelper.HashPassword(password, salt);
            var user = new UserLogin
            {
                Username = username,
                PasswordHash = hash,
                PasswordSalt = salt,
                IsAdmin = isAdmin,
            };

            using (var context = new CyberbreaksDBDataContext(_connectionString))
            {
                context.UserLogins.InsertOnSubmit(user);
                context.SubmitChanges();
            }
        }

        public UserLogin Login(string username, string password)
        {
            using (var context = new CyberbreaksDBDataContext(_connectionString))
            {
                var user = context.UserLogins.FirstOrDefault(u => u.Username == username);
                if (user == null)
                {
                    return null;
                }

                if (!PasswordHelper.PasswordMatch(password, user.PasswordSalt, user.PasswordHash))
                {
                    return null;
                }

                return user;
            }
        }

        public UserLogin GetUser(string username)
        {
            using (var context = new CyberbreaksDBDataContext(_connectionString))
            {
                return context.UserLogins.FirstOrDefault(w => w.Username == username);
            }
        }
    }
}
