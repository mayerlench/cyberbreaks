﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cyberbreaks.Data
{
    public class LoginsRepo
    {
        private string _connectionString;
        public LoginsRepo(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void AddLogin(Login l)
        {
            using (var context = new CyberbreaksDBDataContext(_connectionString))
            {
                context.Logins.InsertOnSubmit(l);
                context.SubmitChanges();
            }
        }

        public IEnumerable<Login> GetLogins()
        {
            using (var context = new CyberbreaksDBDataContext(_connectionString))
            {
               return context.Logins.ToList();
            }
        }

        public void UpdateLogin(Login l)
        {
            using (var context = new CyberbreaksDBDataContext(_connectionString))
            {
                context.Logins.Attach(l);
                context.Refresh(RefreshMode.KeepCurrentValues);
                context.SubmitChanges();
            }
        }

        public void DeleteLogin(int id)
        {
            using (var context = new CyberbreaksDBDataContext(_connectionString))
            {
                context.ExecuteCommand("Delete from Logins where id = {0}", id);
            }
        }
    }
}
